export interface ResCategoriesList {
    Categorias: Categoria[];
}

export interface Categoria {
    id_categoria:     string;
    nombre_categoria: string;
}

export interface ResBrands {
    Marcas:           Marca[];
    Reg_entregados:   number;
    Tot_reg_busqueda: number;
    Tot_Marcas:       number;
}

export interface Marca {
    id_marca:            string;
    nombre_marca:        string;
    logo_marca:          string;
    imagen_marca:        string;
    telefono_marca:      string;
    url_marca:           string;
    vigencia_marca:      Date;
    promo_marca:         string;
    promo_desc_marca:    string;
    restricciones_marca: string;
    categoria_marca:     string;
}
