import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './modules/home/home.component';
import { LoginComponent } from './modules/session/login/login.component';
import { FullComponent } from './layouts/full/full.component';

const routes: Routes = [
  { 
    path: 'home', 
    component: FullComponent, 
    loadChildren: () => import('./modules/home/home.module').then( m => m.HomeModule)
},
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path : '**', redirectTo: 'home', pathMatch: 'full'},

  // { path: '/contact', component: Contact },
];;

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
