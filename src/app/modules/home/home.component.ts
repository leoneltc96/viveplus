import { Component, OnInit } from '@angular/core';
import { Categoria, Marca, ResBrands, ResCategoriesList } from 'src/app/interfaces/home.interface';
import { NgxSpinnerService } from "ngx-spinner";
import { HomeService } from 'src/app/services/home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{
  selectedValue= '' as string;

  categories = [{id_categoria: "",nombre_categoria:""}] as Categoria[];
  brands =[] as Marca[]
//   brands =[{
//     id_marca: "787",
//     nombre_marca: "la casa de los hojaldres",
//     logo_marca: "https://crm.viveplus.com.mx/inc/images/logos_marca/787_logo.jpg",
//     imagen_marca: "https://crm.viveplus.com.mx/inc/images/imagen_marca/787_imagen.jpg",
//     telefono_marca: "5531135871",
//     url_marca: "https://www.casadeloshojaldres.com/",
//     vigencia_marca: "2023-12-31",
//     promo_marca: "10% en cashback en La Casa de los Hojaldres",
//     promo_desc_marca: "10% de cashback en La Casa de los Hojaldres pagando con Bitso Card.",
//     restricciones_marca: "Consumo mínimo $250 pesos. Beneficio topado a $50 pesos por ticket. Aplica únicamente en consumo en sucursal. No aplica en pedidos por Delivery.",
//     categoria_marca: "Restaurante / Comida Rápida"
// }] as Marca[]

  selectedCategory: any = null; 

  constructor(
    private _homeService: HomeService,
    private spinner: NgxSpinnerService,
  ){}

  ngOnInit(): void {
    this.inTakeCategories()
    this.inTakeBrands();
  }
  inTakeCategories() {
    this._homeService.getCategory()
    .subscribe(
      (data: ResCategoriesList) => {
        this.categories = data.Categorias;
      },
      (error) => {
        console.error('Error al obtener datos de la categoría:', error);
      }
    )
  }

  inTakeBrands(){
    this.spinner.show()
    this._homeService.getBrands()
    .subscribe(
      (data: ResBrands) => {
        this.brands = data.Marcas;
        console.log(this.brands); 
        this.spinner.hide()
      },
      (error) => {
        this.spinner.hide()
        console.error('Error al obtener datos de la marca:', error);
      }
    )
  }

  selectCategory(categorie: any) {
    this.selectedCategory = categorie;
    console.log(this.selectedCategory)
  }
}
