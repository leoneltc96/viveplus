import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { FormsModule } from '@angular/forms';

import { MatIconModule } from '@angular/material/icon';
import { HttpClientModule } from '@angular/common/http';
import {MatMenuModule} from '@angular/material/menu';
import { HomeRoutingModule } from './home-routing.module';
import {MatSelectModule} from '@angular/material/select';
import { CardComponent } from 'src/app/components/card/card.component';
import { NgxSpinnerModule } from 'ngx-spinner';







@NgModule({
  declarations: [
    HomeComponent,
    CardComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    HttpClientModule,
    FormsModule,
    MatIconModule,
    MatMenuModule,
    MatSelectModule,
    NgxSpinnerModule
  ]
})
export class HomeModule { }
