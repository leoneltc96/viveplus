import { Injectable } from '@angular/core';
import { AppService } from '../app.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResBrands, ResCategoriesList } from '../interfaces/home.interface';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  private urlServer

  constructor(
    private _appService: AppService,
    private _http: HttpClient,
  ) { 
    this.urlServer = this._appService.urlServers;
  }

  public getCategory(): Observable<ResCategoriesList>{
    var endpoint = 'lista_categorias'
    const params = new HttpParams()
    .set('key', this._appService.apiKey)
    .set('m', endpoint);
    return this._http.get<ResCategoriesList>(this.urlServer, {params})
  }

  public getBrands(): Observable<ResBrands>{
    var endpoint = 'lista_marcas'
    const params = new HttpParams()
    .set('key', this._appService.apiKey)
    .set('m', endpoint);
    return this._http.get<ResBrands>(this.urlServer, {params})
  }


}
