import { Component, Input } from '@angular/core';
import { Marca, ResBrands } from 'src/app/interfaces/home.interface';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {
  @Input()
  public brand= {} as Marca;
  @Input()
  public card = '' as string;


}
