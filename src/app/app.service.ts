import { Injectable } from '@angular/core';
import { environment } from 'src/environment';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  public urlServers = environment.urlServer;
  public apiKey = environment.apiKey;

  constructor() { }
}
